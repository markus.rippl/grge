Configuration
=============


.. autoclass:: grge.config.Config
   :members:


Synchronize external issues
---------------------------

.. autoclass:: grge.config.SyncExternalIssues
   :members:

.. autoclass:: grge.config.ProjectSyncSource
   :members:

.. autoclass:: grge.config.ProjectSyncTarget
   :members:


GitLab Resources
----------------

.. autoclass:: grge.config.GitlabGroup
   :members:

.. autoclass:: grge.config.GitlabProject
   :members:
