# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import pendulum

from . import gitlab


GET = gitlab.GET


class Job(gitlab.Resource):
    async def fetch_trace(self):
        url = f'/projects/{self.project_id}/jobs/{self.id}/trace'
        result = await self._api.call(GET(url), _json=False)
        return result

    @property
    def name(self):
        return self.info['name']

    @property
    def project_id(self):
        return self.info['project_id']

    @property
    def status(self):
        return self.info['status']

    @property
    def web_url(self):
        return self.info['web_url']

    @property
    def stage(self):
        return self.info['stage']

    @property
    def created_at(self) -> pendulum.DateTime:
        created_at = self.info['created_at']
        return pendulum.parse(created_at) if created_at else None  # type: ignore[return-value]

    @property
    def started_at(self) -> pendulum.DateTime:
        started_at = self.info['started_at']
        return pendulum.parse(started_at) if started_at else None  # type: ignore[return-value]

    @property
    def finished_at(self) -> pendulum.DateTime:
        finished_at = self.info['finished_at']
        return pendulum.parse(finished_at) if finished_at else None  # type: ignore[return-value]

    @property
    def delay(self):
        if not self.started_at:
            return None
        return self.started_at - self.created_at

    @property
    def delay_seconds(self):
        delay = self.delay
        return round(delay.total_seconds()) if delay is not None else None

    @property
    def duration(self):
        if not self.started_at or not self.finished_at:
            return None
        return self.finished_at - self.started_at

    @property
    def duration_seconds(self):
        duration = self.duration
        return round(duration.total_seconds()) if duration is not None else None
