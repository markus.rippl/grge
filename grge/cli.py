# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import asyncio
import logging
import os
from collections import OrderedDict
from pathlib import Path
from pprint import pformat

import click
import yarl

from . import config
from . import gitlab
from . import utils
from .daemon import Daemon
from .issue import Issue
from .markdown import join_labels
from .pipeline import Pipeline, Stage
from .project import Project
from .release import Release
from .utils import echo, err


LOGLEVELS = [
    'CRITICAL',  # 50
    'ERROR',  # 40
    'WARNING',  # 30
    'INFO',  # 20
    'DEBUG',  # 10
]


@click.group()
def grge():
    """Gitlab Rocket GEar."""


@grge.command('daemon')
@click.argument('config_file', type=click.Path(exists=True, dir_okay=False))
@click.option(
    '--loglevel', type=click.Choice(LOGLEVELS), default='INFO', show_default=True,
    callback=lambda ctx, param, value: getattr(logging, value), help='Set loglevel.',
)
@click.option('--once', is_flag=True, help='Perform only one synchronization loop then exit.')
@click.option('--dry-run', is_flag=True, help='Print what would happen then exit.')
@utils.asyncio_run
async def grge_daemon(config_file, once, dry_run, loglevel):
    """Gitlab Rocket GEar Daemon."""
    once = once or dry_run

    # Initialise logging, silencing urllib3
    logging.getLogger('urllib3').setLevel(logging.WARNING)
    logging.getLogger().setLevel(loglevel)
    logging.info('Powering up')

    daemon = Daemon(config.load(Path(config_file)))
    daemon.install_signal_handlers()
    await daemon.run(once=once, dry_run=dry_run)


@grge.group('issue')
def grge_issue():
    """Manage GitLab issues."""


@grge_issue.command('rm')
@click.option(
    '--label', 'labels', multiple=True, required=True,
    help='One or more labels needed to match.',
)
@click.option(
    '--state', type=click.Choice(['opened', 'closed']),
    help='Match opened or closed instead of either.',
)
@click.option(
    '--project-url', type=yarl.URL,
    help='Specify project url instead of taking it from current git checkout',
)
@utils.asyncio_run
async def grge_issue_rm(labels, project_url, state):
    """Show issue."""
    if not project_url:
        project_url = utils.get_project_url()
    project_path = '/'.join(project_url.parts[1:])

    async with gitlab.get_api(
            gitlab_url=f'{project_url.scheme}://{project_url.host}',
            auth_token=await utils.get_token(project_url),
    ) as api:
        params = {'labels': ','.join(labels)}
        if state:
            params['state'] = state
        issues = await Issue.search_project(api, project_path, params)
        if not issues:
            err('No matching issues', exit=0)

        for item in issues:
            lines = [
                f'{item.state.upper()} {item.title}',
                f'  {item.web_url}',
                f'  author: {item.author.username}',
                f'  assignees: {", ".join(x.username for x in item.assignees)}',
            ]
            if item.labels:
                lines.append(
                    f'  {join_labels(item.labels)}',
                )
            echo('\n'.join(lines) + '\n')

        click.confirm(f'Do you really want to delete above {len(issues)} issues?', abort=True)
        for item in issues:
            await item.delete()
            echo('.', end='', flush=True)
        echo(f'Deleted {len(issues)} issues')


@grge_issue.command('show')
@click.argument('url', type=yarl.URL)
@utils.asyncio_run
async def grge_issue_show(url):
    """Show issue."""
    async with gitlab.get_api(
            gitlab_url=f'{url.scheme}://{url.host}',
            auth_token=await utils.get_token(url),
    ) as api:
        issue = await Issue.fetch_by_url(api, url)
        echo(pformat(issue.info))


@grge.group('pipeline')
def grge_pipeline():
    """Manage GitLab project pipelines."""


@grge_pipeline.command('timing')
@click.option('--ref', help='Filter for reference (branch/tag)')
@click.option(
    '--project-url', type=yarl.URL,
    help='Specify project url instead of taking it from current git checkout',
)
@utils.asyncio_run
async def grge_pipeline_timing(project_url, ref):
    """Print timing information for pipelines."""
    if not project_url:
        project_url = utils.get_project_url()

    def delta_secs(start, end):
        return round((end - start).total_seconds())

    async with gitlab.get_api(
            gitlab_url=f'{project_url.scheme}://{project_url.host}',
            auth_token=await utils.get_token(project_url),
    ) as api:
        async for pipeline in Pipeline.search(
                api,
                id_or_path='/'.join(project_url.parts[1:]),
                params={'ref': ref} if ref else None,
        ):
            echo(f'\nPipeline {pipeline.web_url}\n')
            stages = OrderedDict()
            await pipeline.refetch_info()
            async for job in pipeline.fetch_jobs():
                stages.setdefault(job.stage, Stage(job.stage)).jobs.append(job)
            table = [
                ('Name', 'Status', 'Create', 'Start', 'Duration'),
            ]
            for name, stage in stages.items():
                create_delay = delta_secs(pipeline.created_at, stage.created_at)
                table.append(
                    (
                        name,
                        '',
                        create_delay,
                        stage.delay_seconds,
                        stage.duration_seconds,
                    ),
                )
                table.extend(
                    (
                        f'{job.name}    ',
                        job.status,
                        delta_secs(stage.created_at, job.created_at),
                        job.delay_seconds,
                        job.duration_seconds,
                    )
                    for job in stage.jobs
                )
            table.append(
                ('TOTAL', pipeline.status, 0, pipeline.delay_seconds, pipeline.duration_seconds),
            )
            table_strs = [
                [str(cell) for cell in row]
                for row in table
            ]
            widths = [
                max(len(cell) for cell in col)
                for col in zip(*table_strs)
            ]
            for row in table_strs:
                echo(' | '.join(f'{cell:>{width}}' for cell, width in zip(row, widths)))
            echo()


@grge.group('release')
def grge_release():
    """Manage GitLab project releases."""


@grge_release.command('create')
@click.option(
    '--changelog', default='CHANGES.rst', type=Path,
    help=(
        'The changelog is expected to be in reStructuredText with sections separated by anchors'
        " (.. <tag_name>:). The first section's tag name is to match the release tag. Its content"
        ' is converted to markdown using pandoc and used as release description.'
    ),
)
@click.option('--confirm/--no-confirm', default=True, show_default=True,
              help='Ask for confirmation')
@click.option('--description', help='Provide description instead of using changelog')
@click.option('--project-url', type=yarl.URL)
@click.option('--external-link', 'external_links', nargs=2, multiple=True,
              help=(
                  'Add external link to release; '
                  'e.g., --external-link grge.md5sum https://link/grge.md5sum'
              ))
@click.argument('tag-name')
@click.argument('files', nargs=-1, type=click.File('rb'))
@utils.asyncio_run
async def grge_release_create(changelog, confirm, description, project_url, external_links,
                              tag_name, files):
    """Create project release.

    Create a release for TAG_NAME consisting of FILES in project at
    PROJECT_URL. The listed files will be uploaded. It is advisable
    but not required to have corresponding sha512sum files for each
    file.
    """
    if not description:
        description = utils.read_changelog_section(changelog.read_text(), tag_name)
        if description is None:
            err(f'Could not find changelog section for {tag_name}', exit=1)
        try:
            description = await utils.rst_to_markdown(description)
        except FileNotFoundError:
            err('ERROR: Pandoc is needed to convert CHANGES.rst section to markdown.')
            err('Install https://pandoc.org/installing.html or provide --description.', exit=1)

    if not project_url:
        project_url = utils.get_project_url()

    token = await utils.get_token(project_url, 'GRGE_RELEASE_TOKEN')
    gitlab_url = f'{project_url.scheme}://{project_url.host}'
    project_path = '/'.join(project_url.parts[1:])
    release_info = {
        'name': f'Release {tag_name}',
        'tag_name': tag_name,
        'description': description,
        'assets': {
            'links': [{'name': name, 'url': url} for name, url in external_links],
        },
    }

    if confirm:
        echo(description)
        click.confirm(
            f'{project_url}: Are you sure you want to create release {tag_name}', abort=True)

    try:
        async with gitlab.get_api(gitlab_url, token) as api:
            project = await Project.fetch_by_path(project_path, api)
            release_info['assets']['links'].extend([
                {
                    'name': upload['alt'],
                    'url': f"{project_url}{upload['url']}",
                }
                for upload in (await asyncio.gather(*[project.upload(x) for x in files]))
            ])
            release = await Release.create(api, project_path, release_info)
            echo(pformat(release.info))
    except gitlab.ApiError as exc:
        err(f'ERROR: {exc!r}')
        err('Has a tag for the release been created and pushed?', exit=1)


@grge_release.command('list')
@click.option('--project-url', type=yarl.URL)
@utils.asyncio_run
async def grge_release_list(project_url):
    """List releases for project."""
    if not project_url:
        project_url = utils.get_project_url()

    token = await utils.get_token(project_url, 'GRGE_RELEASE_TOKEN')
    gitlab_url = f'{project_url.scheme}://{project_url.host}'
    project_path = '/'.join(project_url.parts[1:])
    try:
        async with gitlab.get_api(gitlab_url, token) as api:
            releases = await Release.fetch_all(api, project_path)
            releases.sort(reverse=True, key=lambda x: x.version)
            for release in releases:
                echo(release.tag_name)
    except gitlab.ApiError as exc:
        err(f'ERROR: {exc!r}', exit=1)


@grge_release.command('rm')
@click.option('--confirm/--no-confirm', default=True, show_default=True,
              help='Ask for confirmation')
@click.option('--project-url', type=yarl.URL)
@click.argument('tag_name')
@utils.asyncio_run
async def grge_release_rm(confirm, project_url, tag_name):
    """Remove release from project."""
    if not project_url:
        project_url = utils.get_project_url()

    token = await utils.get_token(project_url, 'GRGE_RELEASE_TOKEN')
    gitlab_url = f'{project_url.scheme}://{project_url.host}'
    project_path = '/'.join(project_url.parts[1:])

    if confirm:
        click.confirm(
            f'{project_url}: Are you sure you want to delete release {tag_name}', abort=True)

    try:
        async with gitlab.get_api(gitlab_url, token) as api:
            release = await Release.fetch_by_tag_name(api, project_path, tag_name)
            await release.delete()
    except gitlab.ApiError as exc:
        err(f'ERROR: {exc!r}', exit=1)


def cli():
    """Setuptools entrypoint."""
    with utils.launch_pdb_on_exception(os.environ.get('PDB')):
        grge(auto_envvar_prefix='GRGE')  # pylint: disable=unexpected-keyword-arg
