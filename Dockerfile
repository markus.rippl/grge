FROM python:3.8

ENV TZ=Europe/Berlin

RUN mkdir /app /src/
WORKDIR /app
COPY . /src

RUN python3.8 -m venv venv
RUN ./venv/bin/pip install -r /src/requirements/production.txt
RUN ./venv/bin/pip install -r /src/requirements/test.txt
RUN ./venv/bin/pip install /src/

RUN cp /src/grge-daemon.yaml.example /app/grge-daemon.yaml
RUN useradd -m grge
USER grge

CMD /app/venv/bin/grge daemon grge-daemon.yaml
