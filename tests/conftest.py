# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import pytest
import pytest_mock
from mock import AsyncMock

from grge.issue import Issue
from grge.user import User


@pytest.fixture
def make_api(mocker):
    def make_api(**kw):  # pylint: disable=redefined-outer-name
        _api = mocker.Mock(spec=[])
        for name, value in kw.items():
            setattr(_api, name, AsyncMock(return_value=value))
        return _api
    return make_api


pytest_mock.MockFixture.AsyncMock = AsyncMock


@pytest.fixture
def api(mocker):
    _api = mocker.Mock()
    _api.call = AsyncMock()
    mocker.patch('grge.gitlab.get_api', return_value=_api)
    return _api


@pytest.fixture
def issue_cls(mocker):
    mocker.patch('grge.issue.Issue.put_and_update', new=AsyncMock())
    mocker.patch('grge.issue.Issue.refetch_info', new=AsyncMock())
    mocker.patch('grge.issue.Issue.search_group', new=AsyncMock())
    mocker.patch('grge.issue.Issue.search_project', new=AsyncMock())
    return Issue


@pytest.fixture
def user_cls(mocker):
    def fetch_by_username(self, username):
        _ = self
        users = {
            'alice': User(None, {'id': 10}),
            'bob': User(None, {'id': 20}),
        }
        return users[username]
    mocker.patch('grge.user.User.fetch_by_username',
                 new=AsyncMock(side_effect=fetch_by_username))
    return User


@pytest.fixture
def issue_search_project(mocker):
    return mocker.patch('grge.issue.Issue.search_project', new=AsyncMock())


@pytest.fixture
def issue_search_group(mocker):
    return mocker.patch('grge.issue.Issue.search_group', new=AsyncMock())


@pytest.fixture
def merge_request_search_project(mocker):
    return mocker.patch('grge.merge_request.MergeRequest.search_project', new=AsyncMock())


@pytest.fixture
def merge_request_search_group(mocker):
    return mocker.patch('grge.merge_request.MergeRequest.search_group', new=AsyncMock())
