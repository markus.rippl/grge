with import <nixpkgs> {};

let
    python = python38;

in

stdenv.mkDerivation rec {
  name = "grge";
  shellHook = ''
    export VENV="$PWD/.venv"
    export XDG_CACHE_HOME="$PWD/.cache"
    unset SOURCE_DATE_EPOCH
    test -d "$VENV" || (${python}/bin/python -m venv "$VENV"
      source "$VENV/bin/activate"
      pip install -U pip~=20.0.2
      pip install -Ur requirements/all.txt
      pip install -c requirements/all.txt --no-binary :all: --force ujson
      flit install -s
    )
    source "$VENV/bin/activate"
    eval "$(_GRGE_COMPLETE=source grge)"
  '';
  buildInputs = [
    libffi
    python
  ];
}
